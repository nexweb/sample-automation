@critical
Feature: 웹 로그인

  @regression @공통 @functional
  Scenario: 유효한 자격 증명으로 로그인 시도
    Given the user is on login page
    When the user enters valid credentials
    And hits submit
    Then the user should be logged in successfully

  @functional @regression
  Scenario: 유효하지 않은 자격 증명으로 로그인 
    Given the user is on login page
    When the user enters invalid credentials
    And hits submit
    Then an invalid credentials error message should be displayed to the user
    And login should be unsuccessful

  @대출 @regression
  Scenario: 로그인 양식에서 회사 로고 확인
    Given the user is on login page
    Then the Company logo should be present in the login form

  @functional @regression @smoke
  Scenario: 비밀번호 찾기 흐름
    Given the user is on login page
    When the user clicks on Forgot Username
    Then the user should be navigated to the first page of forgot username flow
