package com.demo.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAutomationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAutomationApplication.class, args);
	}

}
